﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {
	
	int l_score;
	int r_score;
	bool game_over;
	string end_message;

	// Use this for initialization
	void Start () 
	{
		l_score = 0;
		r_score = 0;
		game_over = false;
	}
	
	void Update()
	{
		if ( l_score >= 10 )
		{
			game_over = true;
			end_message = "Left Player Won!";
		}
		else if ( r_score >= 10 )
		{
			game_over = true;
			end_message = "Right Player Won!";
		}
	}
	public void LeftScored()
	{
		l_score++;
	}
	
	public void RightScored()
	{
		r_score++;
	}
	
	public int[] GetScore()
	{
		int[] scores = {l_score, r_score};
		return scores;
	}
	
	void OnGUI()
	{
		GUIStyle style = new GUIStyle();
		style.fontSize = 50;
		GUI.Label ( new Rect (400, 10, 200, 200),  l_score.ToString(), style);
		GUI.Label ( new Rect (800, 10, 200, 200), r_score.ToString(), style);
		
		if( game_over )
		{
			GUI.Label( new Rect( 450, 300, 200, 200), end_message, style);
			Application.Quit();
		}
	}
	
	IEnumerator EndGame()
	{
		yield return new WaitForSeconds(5);
		Application.Quit();
	}
}
