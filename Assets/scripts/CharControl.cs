﻿using UnityEngine;
using System.Collections;
//[RequireComponent (typeof (CharacterController))]

public class CharControl : MonoBehaviour {
	
//	private CharacterController char_cntrl;
	public bool right_player;
	GameObject this_player;
	float speed = 300.0f;
//	Vector3 moveDirection;
	
	CharacterController _controller;
	GameObject[] my_ctrl_panels;
	GameObject[] nme_ctrl_panels;
	GameObject[] bombs;
	
	
	// Use this for initialization
	void Start () 
	{
		bombs = GameObject.FindGameObjectsWithTag("bomb");
		
		if( right_player )
		{
			this_player = GameObject.Find("player_right");
			my_ctrl_panels = GameObject.FindGameObjectsWithTag("right_control_panel");
			nme_ctrl_panels = GameObject.FindGameObjectsWithTag("left_control_panel");
		}
		else
		{	
			this_player = GameObject.Find ("player_left");
			nme_ctrl_panels = GameObject.FindGameObjectsWithTag("right_control_panel");
			my_ctrl_panels = GameObject.FindGameObjectsWithTag("left_control_panel");
		}
		
		_controller = this_player.GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if ( right_player )
		{
			if ( Input.GetKeyDown( KeyCode.LeftArrow ))
				this.ActivatePanel();
			else if( Input.GetKey(KeyCode.UpArrow))
				this.MoveUp();
			else if ( Input.GetKey(KeyCode.DownArrow))
				this.MoveDown();
		}
		else 
		{
			if ( Input.GetKeyDown( KeyCode.D ))
				this.ActivatePanel();
			else if( Input.GetKey(KeyCode.W))
				this.MoveUp();
			else if ( Input.GetKey(KeyCode.S))
				this.MoveDown();
		}
	}

	public Vector3 GetPosition()
	{
		return this.transform.position;
	}
	
	public void MoveUp()
	{
		_controller.Move( Vector3.forward * speed * Time.deltaTime );
	}
	
	public void MoveDown()
	{
		_controller.Move( Vector3.back * speed * Time.deltaTime );
	}
	
	public void ActivatePanel()
	{
		foreach( GameObject ctrl_panel in my_ctrl_panels )
		{
			float buffer = 20f;
			bool in_range = ( this_player.transform.position.z < ctrl_panel.transform.position.z + buffer && this_player.transform.position.z > ctrl_panel.transform.position.z - buffer ) ? true : false;
				
			if (in_range)
			{
				ctrl_panel.GetComponent<ControlPanel>().Activate();
			}
		}
	}
	
	public Vector3 GetPanelPos ( int cp_num )
	{
		cp_num--;
		return my_ctrl_panels[cp_num].GetComponent<ControlPanel>().GetPosition();
	}
	
	public Vector3 NMEPanelPos ( int nme_panel_num )
	{
		nme_panel_num--;
		return nme_ctrl_panels[nme_panel_num].GetComponent<ControlPanel>().GetPosition();
	}
	
	public ArrayList GetBombInfo ( int bomb_num )
	{
		bomb_num--;
		ArrayList bomb_info = new ArrayList();
		BombBehavior bomb = bombs[bomb_num].GetComponent<BombBehavior>();
		bomb_info.Add( bomb.GetPosition() );
		bomb_info.Add( bomb.GetVelocity() );
		bomb_info.Add( bomb.GetDirection() );
		
		return bomb_info;
	}
}
