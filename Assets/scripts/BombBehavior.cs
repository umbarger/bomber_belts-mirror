﻿using UnityEngine;
using System.Collections;

public class BombBehavior : MonoBehaviour {
	ControlPanel left_panel;
	ControlPanel right_panel;
	float speed = 100f;
	Vector3 starting_position;
	Score scoreboard;
	CharacterController _controller;
	Vector3 last_position;
	Vector3 velocity;
	
	// Use this for initialization
	void Start () 
	{
		starting_position = this.transform.position;
		string bomb_name = this.gameObject.name;
		string bomb_num = bomb_name.Remove(0,4);
		string ctl_panel_r = "r_cp" + bomb_num;
		string ctl_panel_l = "l_cp" + bomb_num;
		last_position = transform.position;
		
		right_panel = GameObject.Find(ctl_panel_r).GetComponent<ControlPanel>();
		left_panel = GameObject.Find(ctl_panel_l).GetComponent<ControlPanel>();
		scoreboard = GameObject.Find("scoreboard").GetComponent<Score>();
		_controller = this.GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		last_position = transform.position;
		
		if( left_panel.IsActivated() )
		{	
			_controller.Move( Vector3.right * speed * Time.deltaTime);
		}
		else if ( right_panel.IsActivated() )
		{
			_controller.Move( Vector3.left * speed * Time.deltaTime );
		}
		
		velocity = last_position - transform.position;
	}
	
	void OnTriggerEnter( Collider col )
	{
		bool hit_panel = ( col.tag == "left_control_panel" || col.tag == "right_control_panel" ) ? true : false;
		
		if ( hit_panel )
		{
			if( right_panel.IsActivated() )
				right_panel.Deactivate();
			if( left_panel.IsActivated() )
				left_panel.Deactivate();
			if( col.tag == "left_control_panel" )
				scoreboard.RightScored();
			else if ( col.tag == "right_control_panel")
				scoreboard.LeftScored();
				
			transform.position = starting_position;
		}
	}
	
	public Vector3 GetPosition()
	{
		return this.transform.position;
	}
	
	public Vector3 GetVelocity()
	{
		return velocity;
	}
	
	public string GetDirection()
	{
		if( right_panel.IsActivated() )
			return "Left";
		else if ( left_panel.IsActivated() )
			return "Right";
		else return "None";
	}
}
